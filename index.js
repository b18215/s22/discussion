// Array Methods

// Mutator Methods

let fruits = ["Apple", "Orange", "Kiwi", "Dragon Fruit"];
console.log(fruits);

// push()
/*
    Syntax:
        arrayName.push("value");
*/

let fruitsLength = fruits.push("Mango");
console.log(fruitsLength);
//result: 5
console.log("Mutated through push: " + fruits);
console.log(fruits);

// add multiple in an array

fruits.push ("Avocado", "Guava");
console.log("Mutated through multiple elements: " + fruits);
console.log(fruits);


// Method for removing (Single Deletion, deletes last item in a row)
// pop()
/*
    Syntax:
        arrayName.pop();
*/


let removedFruit = fruits.pop ();
console.log(removedFruit);
// result: Guava
console.log("Mutated through pop: " + fruits);
console.log(fruits);

// Method to add elements in the begining
// unshift();
/*
    Syntax:
        arrayName.unshift("Value");
*/  


fruits.unshift ("Lime", "Banana");
console.log("Mutated through unshift: " + fruits);
console.log(fruits);


// Method to remove element in the begining
// shift();
/*
    Syntax:
        arrayName.unshift("Value");
*/  


let removedAnotherFruit = fruits.shift();
console.log(removedAnotherFruit);
// result: Lime
console.log("Deleted through shift: " + fruits);
console.log(fruits);


// Re-assigments of elements through Index
// splice();
/*
    Syntax:
        arrayName.splice([index #], [# of item to be added/re-assign], "Value"1, "Value2");
*/  


fruits.splice (1,2, "Lime", "Cherry");
console.log("Mutated through splice: " + fruits);
console.log(fruits);

fruits.splice (3,1, "Orange");
console.log("Mutated through splice: " + fruits);
console.log(fruits);


// Sort; 
/*
    Syntax:
        arrayName.sort();
*/


fruits.sort ();
console.log("Mutated through sort: " + fruits);
console.log(fruits);


// Sort; 
/*
    Syntax:
        arrayName.reverse();
*/


fruits.reverse ();
console.log("Mutated through reverse: " + fruits);
console.log(fruits);




// Splice without Adding
fruits.splice (0,2);
console.log("Mutated through splice without adding: " + fruits);
console.log(fruits);

/*fruits.splice (2,fruits.length);
console.log("Mutated through splice without adding: " + fruits);
console.log(fruits);*/


//Non-Mutator Methods
/*
    indexOf();
        Syntax:
            arrayName.indexOf("searchValue")
*/


let countries = ['US', 'PH', 'CAN', 'SG', 'TH', 'PH', 'FR', 'DE'];
console.log("Non-Mutator Methods: " + countries);
console.log(countries);


let firstIndex = countries.indexOf("PH")
console.log("Result of indexOf validCountry : "+ firstIndex);
console.log(firstIndex);

let invalidCountry = countries.indexOf("BR")
console.log("Result of indexOf invalidCountry : "+ invalidCountry);
console.log(invalidCountry);

// Last Index()
// Returning last index # of the last matching elements
/*
    Syntax:
        arrayName.lastIndexOf("Search Value")
*/


let lastIndex = countries.lastIndexOf("PH")
console.log("Result of lastindexOf: "+ lastIndex);
console.log(lastIndex);
console.log(countries);


// LastIndexOf (Multiple)
/*
    Syntax:
        arrayName.lastIndexOf("Search Value", #target index where to start search)
*/

let LastIndexstart = countries.lastIndexOf("PH", 6);
console.log("Result of lastindexStart: "+ LastIndexstart);
console.log(LastIndexstart);
// Result: Index # 5 (6th item)


// Slice()
/*
    Syntax:
        arrayName.slice(StartingIndex);
        arrayName.slice(StartingIndex, endingindex);

*/

let slicedArrayA = countries.slice(2);
console.log("Result of slice: "+ slicedArrayA);
console.log(slicedArrayA);
console.log("Check list of countries");
console.log(countries);

let slicedArrayB = countries.slice(2, 4);
console.log("Result of slice: "+ slicedArrayB);
console.log(slicedArrayB);
console.log("Check list of countries");
console.log(countries);

let slicedArrayC = countries.slice(-3);
console.log("Result of slice: "+ slicedArrayC);
console.log(slicedArrayC);
console.log("Check list of countries");
console.log(countries);


// toString() - Transforms array to string
/*
    Syntax:
        arrayName.toString()

*/

let stringArray = countries.toString();
console.log("Result from String");
console.log(stringArray);



// concat() - Combining Two Array and Return combined array as result

/*
    Syntax:
        arrayA.concat(arrayB);
        arrayA.concat(elementA);

*/

let taskArrayA = ['drink html', 'eat javascript'];
let taskArrayB = ['inhale CSS', 'breathe bootstrap'];
let taskArrayC = ['get git', 'be node'];


let tasks = taskArrayA.concat(taskArrayB);
console.log("Result of concat method");
console.log(tasks);

let allTasks = taskArrayA.concat(taskArrayB, taskArrayC);
console.log("Result of concat method (Multiple)");
console.log(allTasks);

let combinedTask = taskArrayA.concat('smell express', 'throw react');
console.log("Result of concat method (adding elements)");
console.log(combinedTask);

let tasks2 = taskArrayA.concat(taskArrayB[0]);
console.log("Result of concat method (through index)");
console.log(tasks2);

//join() - Assigning type of spaces before the next element
/*
    Syntax:
        arrayName.join('separator string')
*/


let users = ["John", "Robert", "Jane", "Joe"];
    console.log(users.join()); //result no spaces but with ','
    console.log(users.join('')); //result: no spaces
    console.log(users.join('-')); //result: no spaces


// Iteration Methods

// forEach - Iterate the elements in an Array to a string format
/*
    Syntax:
        arrayName.forEach(function(indivElement)){
            statement / code block
        });
*/

allTasks.forEach(function(task){
    console.log("Result of forEach function/ Iteration Method");
    console.log(task);
});

// Use forEach with conditional statements


let filteredTasks = []
allTasks.forEach(function(task){
    console.log(task);
    if(task.length >10){
        console.log(task);
        filteredTasks.push(task);
    };
});
console.log("Result of filteredTasks:")
console.log(filteredTasks)

// map()
/*
    Syntax:
        let / const resultArray = arrayName.map(function(indivElement){
            return statement/ code block
        })
*/


let numbers = [1, 2, 3, 4, 5, 6];
let numberMap = numbers.map(function(number){
    return number * number;

});

console.log("Original Array:")
console.log(numbers);
console.log("Result of Map Method:")
console.log(numberMap);


let numberForEach = numbers.forEach(function(number){
    return number * number;
});

console.log("Result of numberForEach");
console.log(numberForEach);
// result: Undefined;


// every();
/*
    Syntax:
        let/const resultArray = arrayName.every(function(indivElement){
            return statement/ code block
*/

let allValid = numbers.every(function(number){
    return (number <3);
});

console.log("Result of every() Method");
console.log(allValid);
// result: false


// some();
/*
    Syntax:
        let / const resultArray = arrayName.some(function(){
            return statement / code block (condition)
        });
*/


let someValid = numbers.some(function(number){
    return (number < 2);
});

console.log("Result of someValid Method");
console.log(someValid);

if(someValid){
    console.log("Some numbers in the array are less than 2");
}


// Filter
/*
    Syntax:
        let / const resultArray = arrayName.filter(function(individual Element){
            return statement / code block (condition)
        });
*/

let filterValid = numbers.filter(function(number){
    return (number < 3);
});

console.log("Result of filter Method");
console.log(filterValid);

let nothingFound = numbers.filter(function(number){
    return (number > 5);
});

console.log("Result of filter (nothingFound) Method");
console.log(nothingFound);


/*function filtering(){
    let string = 'supercalifragilisticexpialidocious';
    string.filter(function(i){
        if(
            string[i].toLowerCase() == 'a' ||
            string[i].toLowerCase() == 'e' ||
            string[i].toLowerCase() == 'i' ||
            string[i].toLowerCase() == 'o' ||
            string[i].toLowerCase() == 'u' ||
        ){
            return i
        }
    })
}*/

// filtering using forEach()

let filteredNumbers = [];

numbers.forEach(function(number){
    if (number < 3){
        filteredNumbers.push(number);
    };
});

console.log("Result of filter (forEach) Method");
console.log(filteredNumbers);


//reduce() 
/*
    Syntax:
        let / const resultArray = arrayName.reduce(function(accumulator, currentValue){
            return expression /operation
        });
*/

let iteration = 0;
    console.log('Current Numbers')
    console.log(numbers)
   
let reducedArray = numbers.reduce(function(x, y){
    console.log('start x='+ x)
    console.log('start y='+ y)
    console.warn('current iteration' + ++iteration)
    console.warn('accumulator (x): ' + x);
    console.warn('Current Value (y): ' + y);




    // operation:
    return x + y;
});

console.log('result from reducedArray: ' + reducedArray);


// Used to put with the non-mutator
// includes()
/*
    Syntax:
        arrayName.includes(<argumentToFind>);
*/

let products = ["Mouse", "Keyboard", "Laptop", "Monitor"];

let productFound1 = products.includes('Mouse');
console.log(productFound1);
// result: true


let productFound2 = products.includes('Headset');
console.log(productFound2);
// result: false


let filterProducts = products.filter(function(product){
    return product.toLowerCase().includes('laptop');
})
console.log(filterProducts);